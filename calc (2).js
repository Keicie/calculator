var FKeyPad = document.Keypad; //if you type FKeyPad the computer will read document.Keypad
var Accumulate = 0; // when typing Accumulate it will read 0
var FlagNewNum = false; //when typing FlagNewNum it will read something as false
var PendingOp = ""; // when typing PendingOp it will read as blank or nothing

function NumPressed (Num) { // function tells the computer to do something
	if (FlagNewNum) { // FlagNewNum reads as false
		FKeyPad.ReadOut.value  = Num; // using the variable FKeyPad this literally says to readout/show the value of the number
		FlagNewNum = false; // this just says that false is = to false
	}
	else { // if the "if" statement was not done... it does the "else" statement instead
		if (FKeyPad.ReadOut.value == "0") // if the value reads out 0
			FKeyPad.ReadOut.value = Num; // of the value reads out a num
		else // "else" does something if "if" is not done
			FKeyPad.ReadOut.value += Num; // reads out the value plus or equals the num given
	}
}
function Operation (Op) { // it tells the computer to do mathematical operations
	var Readout = FKeyPad.ReadOut.value // when "Readout" is used the computer reads out the value given
	if (FlagNewNum && PendingOp != "="); // if the new number is equal to the pending operation "="
	else // if "if" doesnt work "else" will do something
	{
		FlagNewNum = true; // FlagNewNum is true
		if ( '+' == PendingOp ) //if plus is equal to the pending operation
			Accumulate += parseFloat(Readout); // accumulate plus or equal to parseFloat
		else if ( '-' == PendingOp ) // "else if" can also be used when "if" doesnt work
			Accumulate -= parseFloat(Readout); // accumulate is minus or equal to parseFloat
		else if ( '/' == PendingOp ) // if divide is equal to the pending operation
			Accumulate /= parseFloat(Readout); // accumulate is divided by or equal to parseFloat
		else if ( '*' == PendingOp ) // if multiply is equal to the pending operation
			Accumulate *= parseFloat(Readout); // accumuate is multiplied by or equal to parseFloat
		else // "else" if "else if" doent work
			Accumulate = parseFloat(Readout); // accumulate is equal to parseFloat
		FKeyPad.ReadOut.value = Accumulate; // the readout value is equal to accumulate
		PendingOp = Op; // pending operation is equal to the operation
	}
}
function makeDecimal () { // makes the entered number a decimal
	var curReadOut = FKeyPad.ReadOut.value; // when curReadOut is used it reads out the value
	if (FlagNewNum) { // if user entered new number
		curReadOut = "0."; // if curReadOut (the value used) is equal to 0
		FlagNewNum = false; // if the new number is equals to false
	}
	else 
	{
		if (curReadOut.indexOf(".") == -1) // when a value is an integer
			curReadOut += "."; // the value is plus or equal to a dot/decimal
	}
	FKeyPad.ReadOut.value = curReadOut; 
}
function ClearEntry () { // clears the user entry
	FKeyPad.ReadOut.value = "0"; // clears readout value
	FlagNewNum = true; // reset new number flag
}
function Clear () { //it clears things
	Accumulate = 0; // accumulate is equal to 0
	PendingOp = ""; // pending operation reads nothing or blank
	ClearEntry(); // clears user entry
}
function Neg () { // makes entry a negative number
	FKeyPad.ReadOut.value = parseFloat(FKeyPad.ReadOut.value) * -1; // multiplies the number to -1 to make a negative number
}
function Percent () { // makes entry a percentage
	FKeyPad.ReadOut.value = (parseFloat(FKeyPad.ReadOut.value) / 100) * parseFloat(Accumulate); // divides a number by 100 to make a percentage
}
function myFunction() { // makes a popup alert
	alert("YASSS I MADE A CALCULATOR!!!"); // pops up the message entered in the quotes
}
